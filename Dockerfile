# Use the lightweight Nginx image
FROM nginx:stable-alpine as production-stage

COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf


COPY /dist /usr/share/nginx/html
# Expose the port 80
EXPOSE 80
# Start Nginx to serve the application
CMD ["nginx", "-g", "daemon off;"]
