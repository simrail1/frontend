import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import * as Sentry from "@sentry/vue";
import toastification from "./plugins/toastification";

import "leaflet/dist/leaflet.css";

Vue.config.productionTip = false;

Sentry.init({
  Vue,
  dsn: "https://e15fb2bb112cc667b81f98c35f404067@o4507283899023360.ingest.de.sentry.io/4507283900465232",
  integrations: [
    Sentry.browserTracingIntegration({ router }),
    Sentry.replayIntegration(),
  ],

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,

  // Set `tracePropagationTargets` to control for which URLs distributed tracing should be enabled
  tracePropagationTargets: [
    "localhost",
    /^https:\/\/simrail\.domiibunn\.live\/api/,
  ],

  // Capture Replay for 10% of all sessions,
  // plus for 100% of sessions with an error
  replaysSessionSampleRate: 0.1,
  replaysOnErrorSampleRate: 1.0,
});

new Vue({
  router,
  vuetify,
  toastification,
  render: (h) => h(App),
}).$mount("#app");
